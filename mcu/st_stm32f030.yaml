name: STM32F103
suffix: F4, K6, C6, C8, CC, R8, RC
variations: TSSOP20, LQFP32, LQFP48, LQFP64
description: ARM Cartex-M0 micro-controller
keywords: IC, MCU, Micro-Controller, STM32, STM32F030
datasheet: https://www.st.com/en/microcontrollers-microprocessors/stm32f030c8.html

pinout@LQFP64:
  PORTA:
    PA0: 14
    PA1: 15
    PA2: 16
    PA3: 17
    PA4: 20
    PA5: 21
    PA6: 22
    PA7: 23
    PA8: 41
    PA9: 42
    PA10: 43
    PA11: 44
    PA12: 45
    PA13-SWDIO: 46
    PA14-SWCLK: 49
    PA15: 50
  PORTB:
    PB0: 26
    PB1: 27
    PB2: 28
    PB3: 55
    PB4: 56
    PB5: 57
    PB6: 58
    PB7: 59
    PB8: 61
    PB9: 62
    PB10: 29
    PB11: 30
    PB12: 33
    PB13: 34
    PB14: 35
    PB15: 36
  PORTC:
    PC0: 8
    PC1: 9
    PC2: 10
    PC3: 11
    PC4: 24
    PC5: 25
    PC6: 37
    PC7: 38
    PC8: 39
    PC9: 40
    PC10: 51
    PC11: 52
    PC12: 53
    PC13: 2
    PC14-OSC32_IN: 3
    PC15-OSC32_OUT: 4
  PORTD:
    PD2: 54
  PORTF:
    PF0-OSC_IN: 5
    PF1-OSC_OUT: 6
    PF4-GND: 18
    PF5-VDD: 19
    PF6-GBD: 47
    PF7-VDD: 48
  CTRL:
    NRST: 7
    BOOT0: 60
  GROUND:
    VSS: 31, 63
    VSSA: 12
  POWER:
    VDD: 1, 32, 64
    VDDA: 13

pinout@LQFP48:
  PORTA:
    PA0: 10
    PA1: 11
    PA2: 12
    PA3: 13
    PA4: 14
    PA5: 15
    PA6: 16
    PA7: 17
    PA8: 29
    PA9: 30
    PA10: 31
    PA11: 32
    PA12: 33
    PA13-SWDIO: 34
    PA14-SWCLK: 37
    PA15: 38
  PORTB:
    PB0: 18
    PB1: 19
    PB2: 20
    PB3: 39
    PB4: 40
    PB5: 41
    PB6: 42
    PB7: 43
    PB8: 45
    PB9: 46
    PB10: 21
    PB11: 22
    PB12: 25
    PB13: 26
    PB14: 27
    PB15: 28
  PORTC:
    PC13: 2
    PC14-OSC32_IN: 3
    PC15-OSC32_OUT: 4
  PORTF:
    PF0-OSC_IN: 5
    PF1-OSC_OUT: 6
    PF6-GBD: 35
    PF7-VDD: 36
  CTRL:
    NRST: 7
    BOOT0: 44
  GROUND:
    VSS: 23, 47
    VSSA: 8
  POWER:
    VDD: 1, 24, 48
    VDDA: 9

pinout@LQFP32:
  PORTA:
    PA0: 6
    PA1: 7
    PA2: 8
    PA3: 9
    PA4: 10
    PA5: 11
    PA6: 12
    PA7: 13
    PA8: 18
    PA9: 19
    PA10: 20
    PA11: 21
    PA12: 22
    PA13-SWDIO: 23
    PA14-SWCLK: 24
    PA15: 25
  PORTB:
    PB0: 14
    PB1: 15
    PB3: 26
    PB4: 27
    PB5: 28
    PB6: 29
    PB7: 30
  PORTF:
    PF0-OSC_IN: 2
    PF1-OSC_OUT: 3
  CTRL:
    NRST: 4
    BOOT0: 31
  GROUND:
    VSS: 16, 32
  POWER:
    VDD: 17, 1
    VDDA: 5

pinout@TSSOP20:
  PORTA:
    PA0: 6
    PA1: 7
    PA2: 8
    PA3: 9
    PA4: 10
    PA5: 11
    PA6: 12
    PA7: 13
    PA9: 17
    PA10: 18
    PA13-SWDIO: 19
    PA14-SWCLK: 20
  PORTB:
    PB1: 14
  PORTF:
    PF0-OSC_IN: 2
    PF1-OSC_OUT: 3
  CTRL:
    NRST: 4
    BOOT0: 1
  GROUND:
    VSS: 15
  POWER:
    VDD: 16
    VDDA: 5

properties:
  in: NRST, BOOT0
  bidir: PORTA, PORTB, PORTC, PORTD, PORTF
  power: VDD, VDDA
  nc: NC
  ground: VSS, VSSA
  inverted: NRST

schematic:
  symbol: IC
  left: CTRL, PORTA, PORTC, PORTF
  right: PORTB, PORTD, PORTE
  top: POWER
  bottom: GROUND

housing@LQFP64:
  suffix: R
  pattern: QFP
  bodyWidth: 10.0 # D1
  bodyLength: 10.0 # E1
  height: 1.6 # A
  leadWidth: 0.17-0.27 # b
  leadLength: 0.45-0.75 # L
  leadSpan: 12.0 # D
  pitch: 0.5 # e
  leadCount: 64
  rowCount: 16
  columnCount: 16

housing@LQFP48:
  suffix: C
  pattern: QFP
  bodyWidth: 6.8-7.2 # D1
  bodyLength: 6.8-7.2 # E1
  height: 1.6 # A
  leadWidth: 0.17-0.27 # b
  leadLength: 0.45-0.75 # L
  leadSpan: 8.8-9.2 # D
  pitch: 0.5 # e
  leadCount: 48
  rowCount: 12
  columnCount: 12

housing@LQFP32:
  suffix: K
  pattern: QFP
  bodyWidth: 6.8-7.2 # D1
  bodyLength: 6.8-7.2 # E1
  height: 1.6 # A
  leadWidth: 0.30-0.45 # b
  leadLength: 0.45-0.75 # L
  leadSpan: 8.8-9.2 # D
  pitch: 0.8 # e
  leadCount: 32
  rowCount: 8
  columnCount: 8

housing@TSSOP20:
  suffix: F
  pattern: SOP
  bodyWidth: 4.30-4.50 # E1
  bodyLength: 6.40-6.60 # D
  height: 1.2 # A
  leadWidth: 0.19-0.30 # b
  leadLength: 0.45-0.75 # L
  leadSpan: 6.20-6.60 # E
  pitch: 0.65 # e
  leadCount: 20
