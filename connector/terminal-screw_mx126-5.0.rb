#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..3).to_a

puts "name: MX126-5.0
description: screw terminal, 5.0 mm pitch
variations: #{variations.collect {|p| p.to_s + 'P'} * ', '}
datasheet: https://datasheet.lcsc.com/lcsc/2309141733_MAX-MX126-5-0-02P-GN01-Cu-S-A_C5188434.pdf

housing: &template
  pattern: custom
  height: 10.3
  bodyLength: 7.8
  leadHeight: 4.0
  leadWidth: 0.6
  leadLength: 1.0
  holeDiameter: 1.3
  pitch: 5.0
  rowCount: 1
  bodyPosition: 0, #{(7.8/2 -4.0).round(2)}
"

variations.each do |v|
  puts "
pinout@#{v}P: 1-#{v}

schematic@#{v}P:
  suffix: -#{v.to_s.rjust(2, '0')}P
  symbol: connector
  left: 1-#{v}

housing@#{v}P:
  suffix: -#{v.to_s.rjust(2, '0')}P
  <<: *template
  bodyWidth: #{(5.0 * v).round(2)}
  silkscreen: M #{(5.0 * v / 2).round(2)}, -3.6, #{(5.0 * v / -2).round(2)}, -3.6
  model: # https://grabcad.com/library/degson-dg126-series-5-0-7-5mm-terminal-block-pack-1
    file: 'library/connector/terminal-screw_mx126-5.0-#{v.to_s.rjust(2, '0')}P.step'
    rotation: -90,0,0
    position: -5.0,3.9,0
  columnCount: #{v}
"
end
