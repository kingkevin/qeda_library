#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (4..30).to_a

puts "name: AFA07"
puts "description: connector, FPC, ZIF, SMD, 1.0 mm pitch"
puts "variations: " + variations.collect{|p| p.to_s+"P"}*", "
puts "datasheet: https://datasheet.lcsc.com/lcsc/1811021315_JUSHUO-AFA07-S06FCC-00_C72727.pdf"
puts "distributors: LCSC C72727"

variations.each do |v|
  puts "pinout@#{v}P: 1-#{v}"
  puts ""
  puts "schematic@#{v}P:"
  puts "  suffix: -S%02dP" % v
  puts "  symbol: connector"
  puts "  left: 1-#{v}"
  puts ""
  puts "housing@#{v}P:"
  puts "  suffix: -S%02dP" % v
  puts "  pattern: custom"
  puts "  bodyWidth: #{(v + 7.0).round(0)} # D"
  puts "  bodyLength: 5.3"
  puts "  height: 2.5"
  puts "  pitch: 1.0"
  puts "  padWidth: 0.6"
  puts "  padHeight: 1.8"
  puts "  columnCount: #{v}"
  puts "  rowCount: 1"
  puts "  rowDY: #{(-5.30 / 2 - 1.80 / 2).round(2)}"
  puts "  # mounting pads"
  puts "  padWidth1: 2.6"
  puts "  padHeight1: 3.0"
  x = ((1.0 * v - 1.0) / -2 - 3.65 + 2.6 / 2).round(2)
  y = (-5.30 / 2 - 1.80 + 4.57 - 3.0 / 2).round(2)
  puts "  padPosition1: #{x},#{y},#{-x},#{y}"
  puts ""
end
