#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..24).to_a

puts "name: MX7620-7.62
description: barrier terminal, 7.62mm pitch
variations: #{variations.collect {|p| p.to_s + 'P'} * ', '}
datasheet: https://datasheet.lcsc.com/lcsc/2309150931_MAX-MX7620-7-62-02P-GN01-Cu-S-A_C5188497.pdf

housing: &template
  pattern: custom
  height: 12.4
  bodyLength: 12.1
  leadHeight: 0.7
  leadWidth: 1.0
  leadLength: 4.2
  holeDiameter: 1.5
  pitch: 7.62
  rowCount: 1
  bodyPosition: 0, #{12.1/2-2.25}
"

variations.each do |v|
  puts "
pinout@#{v}P: 1-#{v}

schematic@#{v}P:
  suffix: -#{v.to_s.rjust(2,'0')}P
  symbol: connector
  left: 1-#{v}

housing@#{v}P:
  suffix: -#{v.to_s.rjust(2,'0')}P
  <<: *template
  silkscreen: M -#{(7.62 * v / 2 + 1.0).round(2)} #{(12.1/2 - 2.25 - 0.5).round(2)} -#{(7.62 * v / 2 + 1.0).round(2)} #{(12.1/2 - 2.25 + 0.5).round(2)} M -#{(7.62 * v / 2 + 1.0).round(2)} #{(12.1/-2 + 2.25 - 0.5).round(2)} -#{(7.62 * v / 2 + 1.0).round(2)} #{(12.1/-2 + 2.25 + 0.5).round(2)}
  bodyWidth: #{(7.62 * v).round(2)}
  model: # from https://grabcad.com/library/terminal-block-7-62mm-2
    file: 'library/connector/terminal-block-7-62mm/Terminal Block - 7.62mm - #{v} Pin.stp'
    rotation: 0,0,180
    position: 0,4.3,0
  columnCount: #{v}
"
end
