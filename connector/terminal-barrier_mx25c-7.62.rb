#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..10).to_a

puts "name: MX25C-7.62
description: barrier terminal, 7.62mm pitch
variations: #{variations.collect {|p| p.to_s + 'P'} * ', '}
datasheet: https://www.lcsc.com/product-detail/Barrier-Terminal-Blocks_MAX-MX25C-7-62-02P-BK01-Cu-S-A_C5188509.html

housing: &template
  pattern: custom
  height: 14.8
  bodyLength: 13.3
  leadHeight: 0.7
  leadWidth: 1.1
  leadLength: 4.0
  holeDiameter: 1.7
  pitch: 7.62
  rowCount: 1
"

variations.each do |v|
  v_pad = v.to_s.rjust(2,'0')
  puts "
pinout@#{v}P: 1-#{v}

schematic@#{v}P:
  suffix: -#{v_pad}P
  symbol: connector
  left: 1-#{v}

housing@#{v}P:
  suffix: -#{v_pad}P
  <<: *template
  bodyWidth: #{((v-1)*7.62+9.38).round(2)}
  model: # from JLCPCB
    file: 'library/connector/terminal-barrier_mx25c-7.62/MX25C-7.62-#{v_pad}P.step'
    rotation: 0,0,0
    position: 7.62,0,0
  columnCount: #{v}
"
end
