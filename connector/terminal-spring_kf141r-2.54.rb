#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..12).to_a

# 3D model form https://grabcad.com/library/kf141r-dg141r-terminal-2-54mm-pitch-1
# X offset is hand adjusted
x = [-5.3] * 13
x[9] = -5.3 - 3 * 2.54

puts "name: KF141R-2.54
description: spring loaded terminal, with lever, 0.1 in/2.54 mm pitch
variations: #{variations.collect {|p| p.to_s + 'P'} * ', '}
datasheet: https://datasheet.lcsc.com/lcsc/2001151504_Cixi-Kefa-Elec-KF141R-2-54-10P_C475132.pdf

housing: &template
  pattern: custom
  height: 12.65
  bodyLength: 13.6
  leadHeight: 3.50
  leadWidth: 0.70
  leadLength: 0.50
  holeDiameter: 1.2
  horizontalPitch: 2.54
  verticalPitch: 5.08
  rowCount: 2
  bodyPosition: #{2.54 - 0.95}, #{13.60/-2+5.08/2+5.5}
"

variations.each do |v|
  puts "
pinout@#{v}P: 1-#{v}

schematic@#{v}P:
  suffix: -#{v}P
  symbol: connector
  left: 1-#{v}

housing@#{v}P:
  suffix: -#{v}P
  <<: *template
  bodyWidth: #{(2.54 * v + 2.54).round(2)}
  model:
    file: 'library/connector/KF141R-2.54/KF141R-2.54-#{v}P.step'
    rotation: 0,0,0
    position: #{(x[v]).round(2)},1.7,0
  columnCount: #{v}
  numbers: #{v.times.collect {|i| [i + 1]}.flatten * 2 * ', '}
"
end
