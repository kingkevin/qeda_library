#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..24).to_a

# X offset is hand adjusted
x = [-5.3] * 13
x[9] = -5.3 - 3 * 2.54

puts "name: DBT50P-6.35
description: barrier block, screw terminal, 6.35mm pitch
variations: #{variations.collect {|p| p.to_s + 'P'} * ', '}
datasheet: https://www.nbdibo.com/wp-content/uploads/2020/07/DBT50P-6.35-XXP-C-P.pdf

housing: &template
  pattern: custom
  height: 12.7
  bodyLength: 14.0
  leadHeight: 3.50
  leadWidth: 1.0
  leadLength: 0.8
  holeDiameter: 1.7
  pitch: 6.36
  rowCount: 1
  bodyPosition: 0, #{14.0/2-4.2}
"

variations.each do |v|
  puts "
pinout@#{v}P: 1-#{v}

schematic@#{v}P:
  suffix: -#{v.to_s.rjust(2,'0')}P
  symbol: connector
  left: 1-#{v}

housing@#{v}P:
  suffix: -#{v.to_s.rjust(2,'0')}P
  <<: *template
  bodyWidth: #{(6.35 * v).round(2)}
  columnCount: #{v}
"
end
