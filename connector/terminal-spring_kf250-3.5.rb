#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..12).to_a

# 3D model x-offset
x_3d_offset = [0.0] * 13
x_3d_offset[3] = 15.7
x_3d_offset[4] = 14.0
x_3d_offset[8] = 7.0

puts "name: KF250-3.5
description: spring loaded terminal, with lever, 3.5 mm pitch, vertical insertion
variations: #{variations.collect {|p| p.to_s + 'P-1'} * ', '}, #{variations.collect {|p| p.to_s + 'P-2'} * ', '}
datasheet: https://a3.ldycdn.com/KF250-3.5-aidqkBpqKirRljSkrnijolli.pdf
distibutors: LCSC C475162

housing: &template
  pattern: custom
  height: 13.3
  bodyLength: 12.1
  bodyPosition: -0.2, #{(12.1/2 - 2.25 - 5.0/2).round(2)}
  leadHeight: 3.6
  leadWidth: 0.85
  leadLength: 0.4
  # top row
  holeDiameter: 1.2
  horizontalPitch: 7.0
  rowCount: 1
  # bottom row
  holeDiameter1: 1.2
  horizontalPitch1: 7.0
  rowCount1: 1
"

variations.each do |v|
v_pad = v.to_s.rjust(2,'0')
  puts "
pinout@#{v}P-1: 1-#{v}

schematic@#{v}P-1:
  suffix: -#{v_pad}P-1
  symbol: connector
  left: 1-#{v}

housing@#{v}P-1:
  suffix: -#{v_pad}P-1
  <<: *template
  bodyWidth: #{(3.5 * v + 1.7).round(2)}
  model:
    file: 'library/connector/terminal-spring_kf250-3.5/KF250-3.5-#{v_pad}P-1.step'
    rotation: 0,0,0
    position: #{x_3d_offset[v]},0,0
  # top row
  columnCount: #{(v/2.0).ceil}
  rowDX: #{v.even? ? -1.75 : 0}
  rowDY: -2.5
  numbers: #{v.times.collect {|i| i+1}.select{|i| 1 == i%2}.flatten * ', '}
  # bottom row
  columnCount1: #{(v/2.0).floor}
  rowDX1: #{v.even? ? 1.75 : 0}
  rowDY1: 2.5
  numbers1: #{v.times.collect {|i| i+1}.select{|i| 0 == i%2}.flatten * ', '}
"
  puts "
pinout@#{v}P-2: 1-#{v}

schematic@#{v}P-2:
  suffix: -#{v_pad}P-2
  symbol: connector
  left: 1-#{v}

housing@#{v}P-2:
  suffix: -#{v_pad}P-2
  <<: *template
  bodyWidth: #{(3.5 * v + 1.7).round(2)}
  model:
    file: 'library/connector/terminal-spring_kf250-3.5/KF250-3.5-#{v_pad}P-2.step'
    rotation: 0,0,0
    position: #{x_3d_offset[v]},0,0
  # top row
  columnCount: #{(v/2.0).ceil}
  rowDX: #{v.even? ? -1.75 : 0}
  rowDY: 2.5
  numbers: #{v.times.collect {|i| i+1}.select{|i| 1 == i%2}.flatten * ', '}
  # bottom row
  columnCount1: #{(v/2.0).floor}
  rowDX1: #{v.even? ? 1.75 : 0}
  rowDY1: -2.5
  numbers1: #{v.times.collect {|i| i+1}.select{|i| 0 == i%2}.flatten * ', '}
"
end
