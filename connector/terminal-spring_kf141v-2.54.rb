#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (2..12).to_a

# X offset is hand adjusted
x = [0] * 13
x[3] = 5.1
x[8] = 11.5

puts "name: KF141V-2.54
description: spring loaded terminal, with lever, 0.1 in/2.54 mm pitch, vertical insertion
variations: #{variations.collect {|p| p.to_s + 'P'} * ', '}
datasheet: https://datasheet.lcsc.com/lcsc/2001151504_Cixi-Kefa-Elec-KF141V-2-54-10P_C475122.pdf

housing: &template
  pattern: custom
  height: 13.0
  bodyLength: 12.65
  leadHeight: 3.50
  leadWidth: 0.70
  leadLength: 0.50
  holeDiameter: 1.2
  horizontalPitch: 2.54
  verticalPitch: 5.08
  rowCount: 2
  bodyPosition: #{2.54 - 0.95}, #{-3.785 + 2.87}
"

variations.each do |v|
  puts "
pinout@#{v}P: 1-#{v}

schematic@#{v}P:
  suffix: -#{v}P
  symbol: connector
  left: 1-#{v}

housing@#{v}P:
  suffix: -#{v}P
  <<: *template
  bodyWidth: #{(2.54 * v + 2.54).round(2)}
  model:
    file: 'library/connector/KF141V-2.54/KF141V-2.54-#{v}P.step'
    rotation: -90,0,-180
    position: #{x[v]},6.3,0
  columnCount: #{v}
  numbers: #{v.times.collect {|i| [i + 1]}.flatten * 2 * ', '}
"
end

# 3D model based on https://grabcad.com/library/pcb-spring-terminal-blocks-dg-141v-kf141v-2-54-1
