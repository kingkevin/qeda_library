#!/usr/bin/env ruby
# encoding: utf-8
# ruby: 3.0.1

# number of possible pins
variations = (3..32).to_a

puts "name: IDC-2.54
description: IDC header, 2 rows, 0.1 in/2.54 mm pitch, through hole, straight, box, shrouded
variations: #{variations.collect {|p| '2x' + p.to_s} * ', '}
keywords: Connector, Male, IDC

housing: &template
  pattern: custom
  options: polarized
  height: 8.8-9.1
  pitch: 2.54
  leadHeight: 3.2-3.4
  leadWidth: 0.64
  leadLength: 0.64
  holeDiameter: 1.0
  columnCount: 2
  bodyWidth: 8.9-9.1

  rowCount1: 1
"

variations.each do |v|
  puts "pinout@2x#{v}: 1-#{v*2}"
  puts

  puts "schematic@2x#{v}:
  suffix: -2x#{v}
  symbol: connector
  left: #{v.times.collect {|p| p*2+1} * ', '}
  right: #{v.times.collect {|p| p*2+2} * ', '}"
  puts

  puts "housing@2x#{v}:
  suffix: -2x#{v}
  <<: *template
  bodyLength: #{(15.24 + (v - 3) * 2.54).round(2)}
  model:
    file: '/usr/share/kicad/3dmodels/Connector_IDC.3dshapes/IDC-Header_2x#{v.to_s.rjust(2,'0')}_P2.54mm_Vertical.step'
    rotation: 0,0,0
    position: -1.27,#{((v - 1) / 2.0 * 2.54).round(2)},0
  rowCount: #{v}"
  puts
end
